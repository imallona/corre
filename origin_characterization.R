if (foo) {
    if (bar) {

         fill_missings_origin <- function(d, annot, tag, selected) {
            if (nrow(d) > 0) {
                
                d <- annot[annot$probe %in% d$probe_origin]
                
                if (tag == 'methylation') return(NULL)
                d$tag <- d[,tag]

                casted <- dcast(d, probe_origin ~ tag)
                ## this fills columns (hmm not present)
                missing <- setdiff(annotation_levels()[[tag]], names(casted))
                missing <- matrix(data = 0, ncol = length(missing), nrow = nrow(casted),
                                  dimnames = list(row.names(casted), missing))

                ids <- casted$probe_origin
                casted <- cbind(casted, missing)

                ## @todo fix here the naturalsort is not needed
                ## (the dict annotation_levels()[[tag]] is already sorted)
                casted <- cbind(ids, casted[naturalsort(setdiff(names(casted), 'probe_origin'))])

                ## this fills rows (cpgs prsent in normal or tumor but not here)
                not_present <- setdiff(selected, casted$ids)
                missing <- matrix(data = 0, nrow = length(not_present), ncol = ncol(casted) - 1)
                
                missing <- cbind(not_present, as.data.frame(missing))
                colnames(missing) <- colnames(casted)
                ## sorting by cpg location
                unsorted <- rbind(casted, missing)
                rownames(unsorted) <- unsorted$ids

                tmp <- annot[annot$probe %in% unsorted$ids,]
                sorted <- unsorted[tmp[order(tmp$distance_closest_tss), 'probe'],]
            } else {
                missing <- matrix(data = 0, ncol = length(annotation_levels()[[tag]]),
                                  nrow = length(selected),
                                  dimnames = list(selected, annotation_levels()[[tag]]))

                missing <- as.data.frame(missing)
                missing <- cbind(selected, missing)

                tmp <- annot[annot$probe %in% missing$selected,]
                sorted <- missing[tmp[order(tmp$distance_closest_tss), 'probe'],]
            }

            return(sorted)
        }

        
    }
}
