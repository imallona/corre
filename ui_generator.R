#!/usr/bin/env R

library(shiny)
## library(networkD3)
## library(shinyTable)

VERSION_INFO = 'a genomic correlations browser (alpha)'
LIPSUM = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed interdum sagittis leo, sit amet pulvinar erat. Vestibulum leo lectus, consectetur a tempus eget, volutpat et libero.'
OFFSET = 2
WIDTH = 8

shinyUI(
    navbarPage(
        title = 'corre',
        theme = 'united.css',
        id = 'main',
        footer = list(
            fluidRow(
                column(
                    width = WIDTH, offset = OFFSET,
                    hr(),
                    p(a(href='http://maplab.cat', 'maplab', target = '_blank'), '2017.',
                      'Developed by',
                      a(href = 'mailto:imallona@igtp.cat', 'Izaskun Mallona.'),
                      'Logo by', a(href = 'mailto:judouet@gmail.com', 'Julien Douet.'),
                      class = "muted", class = "text-center")
                )
            )
        ),
        tabPanel(
            "Browser",
            value = 'meth',
            fluidRow(
                column(
                    width = 4, offset = 2,
                    h3('Gene or probe'),
                    ## textInput('query', 'Probe or gene symbol', value = 'cg13219080'),
                    textInput('query', 'Please introduce a gene symbol or probe', value = 'EN1'),
                    ## selectizeInput('query', 'Please introduce a gene symbol or probe',
                    ##                multiple = FALSE, choices = 'EN1', selected = 'EN1'),
                    actionButton("go", "corre!")#,
                    ## uiOutput('probe_selectinput_ui')

                    ## textOutput('probe_number_of_correlations')
                    
                ), #columnend
                column(
                    width = 4, offset = 0,
                    h3('Cohort'),
                    ## p(LIPSUM),
                    selectInput("cohort_select",
                                label = "Dataset", 
                                choices = list('Colonomics' = 'colonomics_correlations',
                                    'TCGA colon' = 'meth_correlations'),
                                selected = 'colonomics_correlations'), # select input end
                    h3('Filtering'),
                    selectInput("sign",
                                label = "", 
                                choices = list(
                                    'correlating positive (rho >= 0.8)' = 'positive',
                                    'correlating negative (rho < -0.8)' = 'negative',
                                    'correlating positive + negative' = 'any',
                                    ## 'pos+neg' = 'pos+neg',
                                    'anchor CpGs' = 'no_filter'
                                ),
                                selected = 'any') # selectinput end
                    
                    
                ) # column end                
                
            ), #fluidrowend
            
            conditionalPanel(
                ## condition = 'input.go > 0',
                condition = 'output.prefetch_status',
                fluidRow(
                    column(
                        width = 8, offset = 2,
                        h3('Features')
                    ) 
                ), # fluidrow end
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        ## checkboxInput("barplot_relative", "absolute values", FALSE)
                        ## h3('Normal features'),
                        radioButtons("barplot_relative",
                                     label = h4("Quantities"),
                                     choices = list(
                                         "relative" = 'relative',
                                         "absolute" = TRUE),
                                     selected = 'relative',
                                     inline = TRUE)
                    ), # column end
                    column(
                        width = 4, offset = 0,
                        ## h3('Tumor features'),
                        radioButtons("barplot_category",
                                     label = h4("View"),
                                     ## note that these are not implemented
                                     choices = list(
                                         "hmm" = 'hmm',
                                         "methylation" = 'methylation',
                                         "chromosome" = 'chromosome',
                                         "genomic category" = 'genomic_category',
                                         "module" = 'module'
                                     ),
                                     selected = 'hmm',
                                     inline = TRUE)
                    ) # column end
                ), # fluidrow end
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        wellPanel(                           
                            style = "background-color: #ffffff;",
                                h4('Normal'),
                            htmlOutput('barplot_normal')
                        ) # wellpanel end

                        ## textOutput('probe_number_of_correlations')
                        
                    ), #columnend
                    column(
                        width = 4, offset = 0,
                        wellPanel(                           
                            style = "background-color: #ffffff;",
                                h4('Tumor'),
                            htmlOutput('barplot_tumor')
                        ) # wellpanel end

                    ) # column end
                    
                ) # fluidrow end
            ), # conditionalpanel end
            
            conditionalPanel(
                ## condition = 'input.go > 0',
                condition = 'output.prefetch_status',
                fluidRow(
                    
                    column(
                        width = 8, offset = 2,
                        uiOutput('probe_selectinput_ui'),
                        h3 ('Anchor CpG'),
                        ## dataTableOutput(outputId = 'probe_info_dt'),
                        ## dataTableOutput(outputId = 'probe_info_dt'),
                        uiOutput('probe_info_html')
                        
                    )
                 
                ) # conditionalPanel end
            ), # fluidRow end
            ## fluidRow(
            ##     column(
            ##         width = 8, offset = 2,
            ##         h3('old'),
            ##         dataTableOutput(outputId = 'data_table_probe_information')
            ##     ) # column end
            ## ), # fluidrow end

            conditionalPanel(

                condition = "output.valid_status",

                fluidRow(
                    column(
                        width = 8, offset = 2,
                        h3('Annotation')
                    ) 
                ), # fluidrow end
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        radioButtons(
                            "tree_strata",
                            label = br("Annotate by"),
                            choices = list("hmm" = 'hmm', "chromosome" = 'chromosome'),
                            selected = 'hmm',
                            inline = TRUE)                        
                    ),
                    column(
                        width = 4, offset = 0,
                        radioButtons(
                            "plot_type",
                            label = br("Plot as"),
                            choices = list("bubblechart" = 'bubblechart', "treemap" = 'treemap'),
                            selected = 'bubblechart',
                            inline = TRUE)                        
                    )
                    
                ),
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        wellPanel(
                            style = "background-color: #ffffff;",
                                h4('Normal'),
                            conditionalPanel(
                                condition = "input.plot_type == 'bubblechart'",
                                htmlOutput('bubble_normal')
                            ),
                            conditionalPanel(
                                condition = "input.plot_type == 'treemap'",
                                htmlOutput('treemap_normal')
                            )
                        )
                    ),
                    column(
                        width = 4, offset = 0,
                        wellPanel(
                            style = "background-color: #ffffff;",
                                h4('Tumor'),
                            conditionalPanel(
                                condition = "input.plot_type == 'bubblechart'",
                                htmlOutput('bubble_tumor')
                            ),
                            conditionalPanel(
                                condition = "input.plot_type == 'treemap'",
                                htmlOutput('treemap_tumor')
                            )
                        )
                    )
                    
                ), ## fluidRow end
                ## fluidRow(
                ##     column(
                ##         width = 4, offset = 2,
                ##         wellPanel(
                ##             style = "background-color: #ffffff;",
                ##                 h4('Normal'),
                ##             htmlOutput('treemap_normal')
                ##             ## includeScript('tooltip.js')
                ##         )
                ##     ),
                ##     column(
                ##         width = 4, offset = 0,
                ##         wellPanel(
                ##             style = "background-color: #ffffff;",
                ##                 h4('Tumor'),
                ##             htmlOutput('treemap_tumor')
                ##         )
                ##     )
                
                ## ), # fluidrow end
                fluidRow(
                    column(
                        width = 10, offset = 2,
                        h3('Genome distribution of correlating probes')
                    )
                ),
                fluidRow(
                    column(width = 4, offset = 2,
                           selectInput(
                               'circle_chromosome', 'Chromosome',
                               choices = list(
                                   'probe_location' = 'probe_location',
                                   'chr1' = '1',
                                   'chr2' = '2',
                                   'chr3' = '3',
                                   'chr4' = '4',
                                   'chr5' = '5',
                                   'chr6' = '6',
                                   'chr7' = '7',
                                   'chr8' = '8',
                                   'chr9' = '9',
                                   'chr10' = '10',
                                   'chr11' = '11',
                                   'chr12' = '12',
                                   'chr13' = '13',
                                   'chr14' = '14',
                                   'chr15' = '15',
                                   'chr16' = '16',
                                   'chr17' = '17',
                                   'chr18' = '18',
                                   'chr19' = '19',
                                   'chr20' = '20',
                                   'chr21' = '21',
                                   'chr22' = '22'
                               ),
                               selected = 'probe location') # selectinput end
                           ),
                    column (width = 4,
                            selectInput("hmm", "HMM", 
                                        choices = list(
                                            'any' = 'any',
                                            '1_Active_Promoter' = '1_Active_Promoter',
                                            '2_Weak_Promoter' = '2_Weak_Promoter',
                                            '3_Poised_Promoter' = '3_Poised_Promoter',                   
                                            '4_Strong_Enhancer' = '4_Strong_Enhancer',
                                            '5_Strong_Enhancer' = '5_Strong_Enhancer',
                                            '6_Weak_Enhancer' = '6_Weak_Enhancer',
                                            '7_Weak_Enhancer' = '7_Weak_Enhancer',
                                            '8_Insulator' = '8_Insulator',
                                            '9_Txn_Transition' = '9_Txn_Transition',
                                            '10_Txn_Elongation' = '10_Txn_Elongation',
                                            '11_Weak_Txn' = '11_Weak_Txn',
                                            '12_Repressed' = '12_Repressed',
                                            '13_Heterochrom/lo' = '13_Heterochrom/lo',
                                            '14_Repetitive/CNV' = '14_Repetitive/CNV',
                                            '15_Repetitive/CNV' = '15_Repetitive/CNV'),
                                        selected = 'none')
                            
                            )#, #column end
                    ## column(
                    ##     width = 6, offset = 0,
                    ##     plotOutput(outputId = 'circle_intersect', height = 'auto')
                    ## ) # column end
                ), # fluidrow end
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        wellPanel(
                            style = "background-color: #ffffff;",
                                h4('Normal'),
                            plotOutput(outputId = 'circle_by_hmm_normal', height = 'auto'))
                    ),
                    column(
                        width = 4, offset = 0,
                        wellPanel(
                            style = "background-color: #ffffff;",
                                h4('Tumor'),
                            plotOutput(outputId = 'circle_by_hmm_tumor', height = 'auto'))
                    ) # column end
                ), # fluidRow end
                fluidRow(
                    
                    column(
                        width = 8, offset = 2,
                        h3('Correlating probes data')
                        
                    )
                ),
                fluidRow(
                    column(
                        width = 4, offset = 2,
                        ## dataTableOutput('normal_dt'),
                        conditionalPanel(
                            ## 'output.download_normal_status',
                            'output.valid_status',
                            wellPanel(
                                style = "background-color: #ffffff;",
                                    h4('Normal'),
                                dataTableOutput('normal_dt'))
                        )
                    ),
                    column(
                        width = 4, offset = 0,
                        ## dataTableOutput('tumor_dt'),
                        conditionalPanel(
                            ## 'output.download_tumor_status',
                            'output.valid_status',
                            wellPanel(
                                style = "background-color: #ffffff;",
                                    h4('Tumor'),
                                dataTableOutput('tumor_dt'))
                        )

                    ) ## column end
                ), #fluidRow end,
           
            fluidRow(
                
                column(
                    width = 8, offset = 2,
                    h3('Bulk data download'),
                    p('Unfiltered (positive and negative) significant correlations')
                    
                )
            ),
             fluidRow(
                    column(
                        width = 4, offset = 2,
                        ## dataTableOutput('normal_dt'),
                        conditionalPanel(
                            'output.download_normal_status',
                            wellPanel(
                                style = "background-color: #ffffff;",
                                    h4('Normal'),
                              
                                downloadButton('normal_download',
                                               label = "Normal (CSV)"))
                        )
                    ),
                    column(
                        width = 4, offset = 0,
                        ## dataTableOutput('tumor_dt'),
                        conditionalPanel(
                            'output.download_tumor_status',
                            wellPanel(
                                style = "background-color: #ffffff;",
                                    h4('Tumor'),
                               
                                downloadButton('tumor_download',
                                               label = "Tumor (CSV)"))
                        )

                    ) ## column end
             ) #fluidRow end
                ) # conditionalpanel end
            
            ## fluidRow(
            ##     column(
            ##         width = 4, offset = 2,
            ##         ## h3('Dataset'),
            ##         ## textOutput('schema_rendered'),
            ##         h3('Genome plotting'),        
            ##         p('The probe you queried may correlate with probes located in different chromosomes. Intrachromosomal links are allowed; just select the chromosome your starting probe is located at.'),
            ##         selectInput(
            ##             'circle_chromosome', 'Chromosome',
            ##             choices = list(
            ##                 'probe_location' = 'probe_location',
            ##                 'chr1' = '1',
            ##                 'chr2' = '2',
            ##                 'chr3' = '3',
            ##                 'chr4' = '4',
            ##                 'chr5' = '5',
            ##                 'chr6' = '6',
            ##                 'chr7' = '7',
            ##                 'chr8' = '8',
            ##                 'chr9' = '9',
            ##                 'chr10' = '10',
            ##                 'chr11' = '11',
            ##                 'chr12' = '12',
            ##                 'chr13' = '13',
            ##                 'chr14' = '14',
            ##                 'chr15' = '15',
            ##                 'chr16' = '16',
            ##                 'chr17' = '17',
            ##                 'chr18' = '18',
            ##                 'chr19' = '19',
            ##                 'chr20' = '20',
            ##                 'chr21' = '21',
            ##                 'chr22' = '22'
            ##             ),
            ##             selected = 'probe location')#, # selectinput end
            ##         ## plotOutput(outputId = 'circle', height = '500px', width = '500px')
            ##     ), # column end
            ##     column(
            ##         width = 4, offset = 0,
            ##         h3('HMM highlighting'),
            ##         p(LIPSUM),
            ##         selectInput("hmm", "HMM", 
            ##                     choices = list(
            ##                         'none' = 'none',
            ##                         '1_Active_Promoter' = '1_Active_Promoter',
            ##                         '2_Weak_Promoter' = '2_Weak_Promoter',
            ##                         '3_Poised_Promoter' = '3_Poised_Promoter',                   
            ##                         '4_Strong_Enhancer' = '4_Strong_Enhancer',
            ##                         '5_Strong_Enhancer' = '5_Strong_Enhancer',
            ##                         '6_Weak_Enhancer' = '6_Weak_Enhancer',
            ##                         '7_Weak_Enhancer' = '7_Weak_Enhancer',
            ##                         '8_Insulator' = '8_Insulator',
            ##                         '9_Txn_Transition' = '9_Txn_Transition',
            ##                         '10_Txn_Elongation' = '10_Txn_Elongation',
            ##                         '11_Weak_Txn' = '11_Weak_Txn',
            ##                         '12_Repressed' = '12_Repressed',
            ##                         '13_Heterochrom/lo' = '13_Heterochrom/lo',
            ##                         '14_Repetitive/CNV' = '14_Repetitive/CNV',
            ##                         '15_Repetitive/CNV' = '15_Repetitive/CNV'),
            ##                     selected = 'none')#,
            ##         ## p('The hmm selected is'),
            ##         ## textOutput('hmm_rendered'),
            ##         ## plotOutput(outputId = 'circle_by_hmm', height = '500px',
            ##                    ## width = '500px')
            ##     ), # column end
            ##     column(width = WIDTH, offset = OFFSET,
            ##            h3('Probe information'),
            ##            p('The probe you entered has the following characteristics'),
            ##            dataTableOutput(outputId = 'data_table_probe_information'),
            ##            h3('Correlations sum-up'),
            ##            p('The correlations found are'),
            ##            dataTableOutput(outputId = 'data_table_probe'),
            ##            br()
            ##            ) # column end
            ## ) # fluid row end
        ), # tabpanel end
        tabPanel(
            'About',
            fluidRow(
                column(
                    width = 8, offset = 2,
                    h2('Aim'),
                    p('Epigenetic alterations are a widespread signature of human cancer, including generalized changes in DNA methylation status and variability. Whilst frequently affecting small to large regions of co-localized CpGs (i.e. CpG island shores), non adjacent loci also co-methylate.'),
                    p('To evaluate this phenomenon we carried out a serial correlation analysis on the Infinium 450k array using two independent colon cancer cohorts.'),
                    h2('Quick start'),
                    p("Introduce a gene symbol (i.e. EN1) and press the corre! button. This will provide the strong co-methylations present at the Colonomics series. By default, the data retrieved corresponds to effect size Spearman's rho greater or equal than 0.8 or less than -0.8."),
                    p('Corre will generate multiple panels with the normal and tumor data side by side.'),
                    h3('Features panel'),
                    p('The features panel renders the co-methylation landscape of each of the CpGs placed near (or at) the EN1 gene (by default, the chromatin colors distribution).'),
                    p('The barplot can be updated to switch from relative quantities (by default) to absolute numbers by clicking into the Quantities checkboxes. The annotation layers, such as chromosome, genomic category (i.e. exon, intron etc) and co-methylation module can be readily updated by clicking into the checkboxes on the right.'),
                    p('The methylation status can be rendered as a quantile summarization of the DNA methylation status of the CpGs. In brief, each color depicts the abundance of samples belonging to five strata of DNA methylation, from nearly almost unmethylated (left, light gray with beta values between 0 and 0.2) to almost totally methylated (right, black, beta vlues over 0.8)'),
                    h3('Anchor probe selection'),
                    p('By default, the most upstream CpG to the queried gene will be taken as anchor CpG (i.e. the one being checked for co-methylation to the whole genome). The anchor can be easily updated by selecting the most convenient probe from the select input'),
                    h3('Annotation'),
                    p('Co-methylations can be summarized by either their chromosome or its chromatin color (Ernst 15 chromatin states segmentation by HMM run in hESC H1).'),
                    p("Bubblecharts represent the log2 fold change (enrichment) of your query's co-methylationsas compared to what would be expected from random sampling from the Infinium 450k background."),
                    p("Treemaps represent the co-methylations as simple counts. You can browse within the treemap by right-clicking (zooms in) o left-clicking (goes back)."),
                    h3("Genome distribution"),
                    p('Circular views depict intra-chromosomal co-methylations in red and trans-comethylations in blue.'),
                    h4("Data"),
                    p('Spreadsheets can be downloaded as comma-separated plain text files (CSVs) with header. Each row depicts a co-methylation; annotations refer to the probe the anchor CpG correlates with.'),
                    
                    h2('Method and data sources'),
                    p('Colonomics series included primary tumors and their adjacent normal tissue from microsatellite-stable samples at stage II (82 at IIA and 8 at IIB) from patients, 67 males and 23 females, aged 43-86 years (mean 70.37); 20 developed metastasis.'),
                    p('The Cancer Genome Atlas (TCGA) series was composed by 256 primary tumor and 38 adjacent normal samples from the colon adenocarcinoma (COAD) cohort from patients aged 31 to 90 years at diagnosis (mean 65.61), being 141 males, 144 females and one unassigned. Pathologic stages included Stage I (40), Stage II (97), Stage III (75), Stage IV (32); 11 were not available or discrepant. Regarding microsatellite instability, 10 were positive, 65 negative and 181 were either not tested or had an unknown status.'),
                    h3('External links'),
                    tags$ul(
                        tags$li(
                            a(href= 'https://www.colonomics.org/', 'Colonomics', target="_blank")
                        ),
                        tags$li(a(href= 'https://portal.gdc.cancer.gov/', 'TCGA GDC',
                                  target="_blank"))),
                    h3('Making off'),
                    p('Corre is built with R/shiny and imports the following R packages:'),
                    tags$ul(
                        tags$li(
                            a(href= 'https://cran.r-project.org/web/packages/RPostgreSQL/index.html', 'RPostgreSQL', target="_blank")
                        ),
                        tags$li(
                            a(href= 'https://cran.r-project.org/web/packages/RCircos/index.html', 'RCircos', target="_blank")
                        ),
                        tags$li(
                            a(href= 'https://cran.r-project.org/web/packages/googleVis/index.html', 'googleVis', target="_blank")
                        )),

                    HTML(
                        '<h3>Contact</h3>
            <p>
              Any feedback is welcome!
              Please contact
              <a href="mailto:imallona@igtp.cat">Izaskun Mallona</a>
              or
              <a href="mailto:mpeinado@igtp.cat">Miguel A. Peinado.</a>
            </p>
            <p>Corre was developed at the Miguel A. Peinado&#39;s lab.
              The main focus of our research is the characterization of the molecular mechanisms underlying cancer cell programs and the identification of molecular markers with clinical applications.</p>
            <p>
              Please check more details on the lab and our toolshed at
              <a href="http://maplab.cat">maplab.cat.</a>
            </p>
            <p>We are continuously releasing new epigenetics data visualization and integration tools. You might also like:</p>
            <ol>
              <li>
                <a href="http://maplab.cat/methylation_plotter/" target="_blank">Methylation plotter,</a>
                a web tool for dynamic visualization of DNA methylation data
                <a href="http://scfbm.biomedcentral.com/articles/10.1186/1751-0473-9-11" target="_blank">(Source Code for Biology and Medicine 2014, 9:11).</a>
              </li>
              <li>
                <a href="http://www.maplab.cat/wanderer" target="_blank">Wanderer,</a>
                an interactive viewer to explore DNA methylation and gene expression data in human cancer
                <a href="http://www.epigeneticsandchromatin.com/content/8/1/22" target="_blank">(Epigenetics &amp; Chromatin 2015, 8:22).</a>
              </li>
              <li>
                <a href="http://www.maplab.cat/alu_ontology.html" target="_blank">The Alu knowledgebase,</a>
                an ontology of the human Alu repeats.
                <a href="http://www.sciencedirect.com/science/article/pii/S1532046416000113" target="_blank">(Journal of Biomedical Informatics 2016, 60:77-83)</a>                
              </li>
              <li>
                <a href="http://www.maplab.cat/chainy" target="_blank">Chainy,</a>
                a QPCR workflow.
                <a href="http://dx.doi.org/10.1093/bioinformatics/btw839" target="_blank">(Bioinformatics, 2017)</a>                
              </li>
              <li>
                <a href="http://www.maplab.cat/truke" target="_blank">Truke,</a>
                a tool to reshape structured data and look for gene symbols corrupted by Excel misuse.
                <a href="http://dx.doi.org/10.1186/s12864-017-3631-8" target="_blank">(BMC Genomics 2017, 18:242)</a>                
              </li>
'
                    )
                )  # column end
            ) # fluid row end
        ) #tabpanel end
    ) #navbarpage end
)
